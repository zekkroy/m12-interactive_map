# Title: Interactive Coronavirus Map
# Author: Daniel Gracia Morales

library(dplyr)

csv_data <- tryCatch(
  
  {
    
    # Confirmed coronavirus cases.
    confirmed_cases <- read.csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")
    
    
    # Death coronavirus cases.
    death_cases <- read.csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv")
    
    
    # Recovered coronavirus cases.
    recovered_cases <- read.csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv")
    
    
    write.csv(confirmed_cases, paste0(getwd(), "/covid_data/confirmed_cases.csv"), row.names = FALSE)
    
    write.csv(death_cases, paste0(getwd(), "/covid_data/death_cases.csv"), row.names = FALSE)
    
    write.csv(recovered_cases, paste0(getwd(), "/covid_data/recovered_cases.csv"), row.names = FALSE)
    
  },
  
  error = function(cond) {
    
    print("Error getting data and writting CSV files.")
    print("Here's the original error message:")
    print(cond)
    
  },
  
  warning = function(cond) {
    
    print("Error getting data and writting CSV files.")
    print("Here's the original warning message:")
    print(cond)
    
  }
)


