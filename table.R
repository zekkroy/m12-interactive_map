# Title: Interactive Coronavirus Map
# Author: Daniel Gracia Morales

library(dplyr)



table_data <- function (selected_date, confirmed_cases, death_cases, recovered_cases) {
  
  covid_cases <- confirmed_cases %>%
    select(1:4)
  
  confirmed_cases_date <- confirmed_cases %>%
    select(selected_date)
  
  colnames(confirmed_cases_date)[1] <- "Confirmed"
  
  death_cases_date <- death_cases %>%
    select(selected_date)
  
  colnames(death_cases_date)[1] <- "Death"
  
  recovered_cases_date <- recovered_cases %>%
    select(Country.Region, selected_date)
  
  colnames(recovered_cases_date)[2] <- "Recovered"
  
  recovered_cases_date <- recovered_cases_date %>%
    group_by(Country.Region) %>%
    summarise("Recovered Cases" = sum(Recovered)) %>%
    select(2)
  
  covid_cases <-
    cbind(covid_cases, confirmed_cases_date, death_cases_date)
  
  covid_cases <- covid_cases %>%
    group_by(Country.Region) %>%
    summarise("Confirmed Cases" = sum(Confirmed), "Death Cases" = sum(Death))
  
  covid_cases <- cbind(covid_cases, recovered_cases_date)
  
  colnames(covid_cases)[1] <- "Country"
  
  covid_cases
  
}

table <- function (selected_date, confirmed_cases, death_cases, recovered_cases) {
  
  table_data <- table_data(selected_date, confirmed_cases, death_cases, recovered_cases)
  
  table <- DT::renderDataTable(table_data,  rownames = FALSE, filter = list(position = "top", clear = TRUE), options = list(pageLength = nrow(table_data), lengthChange = FALSE, dom = "t", order = list(1, 'desc')))
  
}
