# My Interactive Map related with Covid-19

This project is about an Interactive Map about Covid-19 made it with R Shiny and HTML.

## Getting Starting

_These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system._

## Installing

This project, as it said before, is made in R Shiny. For execute it, you will need to download and install R (if you don't have it yet). You can download it with this URL https://cran.r-project.org/ .
<br>
<br>
If you're using Linux or another GNU operating system, you can download it from terminal with this command:

```
sudo apt-get update
sudo apt-get install r-base
```

## Execute R Shiny

When R is installed successfully and operable, execute "app.R" file in the project folder. This script will run all the interactive map on http://map.html/ iframe. 

```
RScript app.R
```

# Important Notes

R Shiny is implemented in HTML with iframe. You need to change in file "map.html" the next line with your local IP and port 8100:

```
<iframe src=" "":8100 " , style="width: 100%; height: 800px; border: 0;"></iframe>
```
