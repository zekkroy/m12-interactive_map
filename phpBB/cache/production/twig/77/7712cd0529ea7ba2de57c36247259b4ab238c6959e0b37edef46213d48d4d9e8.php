<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* navbar_responsive_header.html */
class __TwigTemplate_b63f85447d2be45f62589d2f940f20524300f05aa57ee170169a2036e27a4746 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"dropdown-container hidden inventea-mobile-dropdown-menu\">
\t<a href=\"#\" class=\"dropdown-trigger inventea-toggle\"><i class=\"icon fa fa-bars\"></i></a>
\t<div class=\"dropdown hidden\">
\t<div class=\"pointer\"><div class=\"pointer-inner\"></div></div>
\t<ul class=\"dropdown-contents\" role=\"menubar\">

\t\t";
        // line 7
        if (($context["U_SITE_HOME"] ?? null)) {
            // line 8
            echo "\t\t\t<li>
\t\t\t\t<a href=\"";
            // line 9
            echo ($context["U_SITE_HOME"] ?? null);
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa fa-fw fa-home\" aria-hidden=\"true\"></i>
\t\t\t\t\t<span>";
            // line 11
            echo $this->extensions['phpbb\template\twig\extension']->lang("SITE_HOME");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t";
        }
        // line 15
        echo "
\t\t<li>
\t\t\t<a href=\"";
        // line 17
        echo ($context["U_INDEX"] ?? null);
        echo "\" role=\"menuitem\">
\t\t\t\t<i class=\"icon fa fa-fw ";
        // line 18
        if (($context["U_SITE_HOME"] ?? null)) {
            echo "fa-globe";
        } else {
            echo "fa-home";
        }
        echo "\" aria-hidden=\"true\"></i>
\t\t\t\t<span>";
        // line 19
        echo $this->extensions['phpbb\template\twig\extension']->lang("INDEX");
        echo "</span>
\t\t\t</a>
\t\t</li>\t\t
\t\t";
        // line 22
        // line 23
        echo "
\t\t<li ";
        // line 24
        if ( !($context["S_USER_LOGGED_IN"] ?? null)) {
            echo "data-skip-responsive=\"true\"";
        } else {
            echo "data-last-responsive=\"true\"";
        }
        echo ">
\t\t\t<a id=\"menu_faq\" href=\"";
        // line 25
        echo ($context["U_FAQ"] ?? null);
        echo "\" rel=\"help\" title=\"";
        echo $this->extensions['phpbb\template\twig\extension']->lang("FAQ_EXPLAIN");
        echo "\" role=\"menuitem\">
\t\t\t\t<i class=\"icon fa-question-circle fa-fw\" aria-hidden=\"true\"></i><span>";
        // line 26
        echo $this->extensions['phpbb\template\twig\extension']->lang("FAQ");
        echo "</span>
\t\t\t</a>
\t\t</li>
\t\t";
        // line 29
        // line 30
        echo "
\t\t";
        // line 31
        if (($context["S_DISPLAY_SEARCH"] ?? null)) {
            // line 32
            echo "\t\t\t<li>
\t\t\t\t<a href=\"";
            // line 33
            echo ($context["U_SEARCH"] ?? null);
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa-search fa-fw\" aria-hidden=\"true\"></i><span>";
            // line 34
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>

\t\t\t";
            // line 38
            if (($context["S_REGISTERED_USER"] ?? null)) {
                // line 39
                echo "\t\t\t\t<li>
\t\t\t\t\t<a href=\"";
                // line 40
                echo ($context["U_SEARCH_SELF"] ?? null);
                echo "\" role=\"menuitem\">
\t\t\t\t\t\t<i class=\"icon fa-file-o fa-fw icon-gray\" aria-hidden=\"true\"></i><span>";
                // line 41
                echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_SELF");
                echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
            }
            // line 45
            echo "
\t\t\t";
            // line 46
            if (($context["S_USER_LOGGED_IN"] ?? null)) {
                // line 47
                echo "\t\t\t\t<li>
\t\t\t\t\t<a href=\"";
                // line 48
                echo ($context["U_SEARCH_NEW"] ?? null);
                echo "\" role=\"menuitem\">
\t\t\t\t\t\t<i class=\"icon fa-file-o fa-fw icon-red\" aria-hidden=\"true\"></i><span>";
                // line 49
                echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_NEW");
                echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
            }
            // line 53
            echo "
\t\t\t";
            // line 54
            if (($context["S_LOAD_UNREADS"] ?? null)) {
                // line 55
                echo "\t\t\t\t<li>
\t\t\t\t\t<a href=\"";
                // line 56
                echo ($context["U_SEARCH_UNREAD"] ?? null);
                echo "\" role=\"menuitem\">
\t\t\t\t\t\t<i class=\"icon fa-file-o fa-fw icon-red\" aria-hidden=\"true\"></i><span>";
                // line 57
                echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_UNREAD");
                echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
            }
            // line 60
            echo "\t

\t\t\t<li>
\t\t\t\t<a href=\"";
            // line 63
            echo ($context["U_SEARCH_UNANSWERED"] ?? null);
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa-file-o fa-fw icon-gray\" aria-hidden=\"true\"></i><span>";
            // line 64
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_UNANSWERED");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li>
\t\t\t\t<a href=\"";
            // line 68
            echo ($context["U_SEARCH_ACTIVE_TOPICS"] ?? null);
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa-file-o fa-fw icon-blue\" aria-hidden=\"true\"></i><span>";
            // line 69
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_ACTIVE_TOPICS");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t\t<li class=\"separator\"></li>
\t\t";
        }
        // line 74
        echo "
\t\t";
        // line 75
        // line 76
        echo "
\t\t";
        // line 77
        if ( !($context["S_IS_BOT"] ?? null)) {
            // line 78
            echo "\t\t\t";
            if (($context["S_DISPLAY_MEMBERLIST"] ?? null)) {
                // line 79
                echo "\t\t\t\t<li id=\"menu_memberlist\">
\t\t\t\t\t<a href=\"";
                // line 80
                echo ($context["U_MEMBERLIST"] ?? null);
                echo "\" role=\"menuitem\">
\t\t\t\t\t\t<i class=\"icon fa-group fa-fw\" aria-hidden=\"true\"></i><span>";
                // line 81
                echo $this->extensions['phpbb\template\twig\extension']->lang("MEMBERLIST");
                echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
            }
            // line 85
            echo "\t\t\t";
            if (($context["U_TEAM"] ?? null)) {
                // line 86
                echo "\t\t\t\t<li id=\"menu_team\">
\t\t\t\t\t<a href=\"";
                // line 87
                echo ($context["U_TEAM"] ?? null);
                echo "\" role=\"menuitem\">
\t\t\t\t\t\t<i class=\"icon fa-shield fa-fw\" aria-hidden=\"true\"></i><span>";
                // line 88
                echo $this->extensions['phpbb\template\twig\extension']->lang("THE_TEAM");
                echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t";
            }
            // line 92
            echo "\t\t";
        }
        // line 93
        echo "
\t\t";
        // line 94
        if (($context["U_ACP"] ?? null)) {
            // line 95
            echo "\t\t\t<li data-last-responsive=\"true\">
\t\t\t\t<a href=\"";
            // line 96
            echo ($context["U_ACP"] ?? null);
            echo "\" title=\"";
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP");
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa-cogs fa-fw\" aria-hidden=\"true\"></i><span>";
            // line 97
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SHORT");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t";
        }
        // line 101
        echo "\t\t";
        if (($context["U_MCP"] ?? null)) {
            // line 102
            echo "\t\t\t<li data-last-responsive=\"true\">
\t\t\t\t<a href=\"";
            // line 103
            echo ($context["U_MCP"] ?? null);
            echo "\" title=\"";
            echo $this->extensions['phpbb\template\twig\extension']->lang("MCP");
            echo "\" role=\"menuitem\">
\t\t\t\t\t<i class=\"icon fa-gavel fa-fw\" aria-hidden=\"true\"></i><span>";
            // line 104
            echo $this->extensions['phpbb\template\twig\extension']->lang("MCP_SHORT");
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t";
        }
        // line 108
        echo "            ";
        // line 109
        echo "        </ul>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "navbar_responsive_header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 109,  296 => 108,  289 => 104,  283 => 103,  280 => 102,  277 => 101,  270 => 97,  264 => 96,  261 => 95,  259 => 94,  256 => 93,  253 => 92,  246 => 88,  242 => 87,  239 => 86,  236 => 85,  229 => 81,  225 => 80,  222 => 79,  219 => 78,  217 => 77,  214 => 76,  213 => 75,  210 => 74,  202 => 69,  198 => 68,  191 => 64,  187 => 63,  182 => 60,  175 => 57,  171 => 56,  168 => 55,  166 => 54,  163 => 53,  156 => 49,  152 => 48,  149 => 47,  147 => 46,  144 => 45,  137 => 41,  133 => 40,  130 => 39,  128 => 38,  121 => 34,  117 => 33,  114 => 32,  112 => 31,  109 => 30,  108 => 29,  102 => 26,  96 => 25,  88 => 24,  85 => 23,  84 => 22,  78 => 19,  70 => 18,  66 => 17,  62 => 15,  55 => 11,  50 => 9,  47 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "navbar_responsive_header.html", "");
    }
}
