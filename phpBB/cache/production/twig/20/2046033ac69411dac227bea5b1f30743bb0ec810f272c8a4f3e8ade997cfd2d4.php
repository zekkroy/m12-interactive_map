<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* acp_search.html */
class __TwigTemplate_08f459b73dc1211b202f8b161207482db649f2c930a44439fa705f79c570eeeb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "acp_search.html", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "
<a id=\"maincontent\"></a>

";
        // line 5
        if (($context["S_SETTINGS"] ?? null)) {
            // line 6
            echo "\t<h1>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SEARCH_SETTINGS");
            echo "</h1>

\t<p>";
            // line 8
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SEARCH_SETTINGS_EXPLAIN");
            echo "</p>

\t<form id=\"acp_search\" method=\"post\" action=\"";
            // line 10
            echo ($context["U_ACTION"] ?? null);
            echo "\">

\t<fieldset>
\t\t<legend>";
            // line 13
            echo $this->extensions['phpbb\template\twig\extension']->lang("GENERAL_SEARCH_SETTINGS");
            echo "</legend>
\t<dl>
\t\t<dt><label for=\"load_search\">";
            // line 15
            echo $this->extensions['phpbb\template\twig\extension']->lang("YES_SEARCH");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("YES_SEARCH_EXPLAIN");
            echo "</span></dt>
\t\t<dd><label><input type=\"radio\" class=\"radio\" id=\"load_search\" name=\"config[load_search]\" value=\"1\"";
            // line 16
            if (($context["S_YES_SEARCH"] ?? null)) {
                echo " checked=\"checked\"";
            }
            echo " /> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("YES");
            echo "</label>
\t\t\t<label><input type=\"radio\" class=\"radio\" name=\"config[load_search]\" value=\"0\"";
            // line 17
            if ( !($context["S_YES_SEARCH"] ?? null)) {
                echo " checked=\"checked\"";
            }
            echo " /> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("NO");
            echo "</label></dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"search_interval\">";
            // line 20
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_INTERVAL");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_INTERVAL_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"search_interval\" type=\"number\" min=\"0\" max=\"9999\" name=\"config[search_interval]\" value=\"";
            // line 21
            echo ($context["SEARCH_INTERVAL"] ?? null);
            echo "\" /> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SECONDS");
            echo "</dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"search_anonymous_interval\">";
            // line 24
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_GUEST_INTERVAL");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_GUEST_INTERVAL_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"search_anonymous_interval\" type=\"number\" min=\"0\" max=\"9999\" name=\"config[search_anonymous_interval]\" value=\"";
            // line 25
            echo ($context["SEARCH_GUEST_INTERVAL"] ?? null);
            echo "\" /> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SECONDS");
            echo "</dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"limit_search_load\">";
            // line 28
            echo $this->extensions['phpbb\template\twig\extension']->lang("LIMIT_SEARCH_LOAD");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("LIMIT_SEARCH_LOAD_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"limit_search_load\" type=\"text\" size=\"4\" maxlength=\"4\" name=\"config[limit_search_load]\" value=\"";
            // line 29
            echo ($context["LIMIT_SEARCH_LOAD"] ?? null);
            echo "\" /></dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"min_search_author_chars\">";
            // line 32
            echo $this->extensions['phpbb\template\twig\extension']->lang("MIN_SEARCH_AUTHOR_CHARS");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("MIN_SEARCH_AUTHOR_CHARS_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"min_search_author_chars\" type=\"number\" min=\"0\" max=\"9999\" name=\"config[min_search_author_chars]\" value=\"";
            // line 33
            echo ($context["MIN_SEARCH_AUTHOR_CHARS"] ?? null);
            echo "\" /></dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"max_num_search_keywords\">";
            // line 36
            echo $this->extensions['phpbb\template\twig\extension']->lang("MAX_NUM_SEARCH_KEYWORDS");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("MAX_NUM_SEARCH_KEYWORDS_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"max_num_search_keywords\" type=\"number\" min=\"0\" max=\"9999\" name=\"config[max_num_search_keywords]\" value=\"";
            // line 37
            echo ($context["MAX_NUM_SEARCH_KEYWORDS"] ?? null);
            echo "\" /></dd>
\t</dl>
\t<dl>
\t\t<dt><label for=\"search_store_results\">";
            // line 40
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_STORE_RESULTS");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_STORE_RESULTS_EXPLAIN");
            echo "</span></dt>
\t\t<dd><input id=\"search_store_results\" type=\"number\" min=\"0\" max=\"999999\" name=\"config[search_store_results]\" value=\"";
            // line 41
            echo ($context["SEARCH_STORE_RESULTS"] ?? null);
            echo "\" /> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SECONDS");
            echo "</dd>
\t</dl>
\t</fieldset>

\t<fieldset>
\t\t<legend>";
            // line 46
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_TYPE");
            echo "</legend>
\t<dl>
\t\t<dt><label for=\"search_type\">";
            // line 48
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_TYPE");
            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
            echo "</label><br /><span>";
            echo $this->extensions['phpbb\template\twig\extension']->lang("SEARCH_TYPE_EXPLAIN");
            echo "</span></dt>
\t\t<dd><select id=\"search_type\" name=\"config[search_type]\" data-togglable-settings=\"true\">";
            // line 49
            echo ($context["S_SEARCH_TYPES"] ?? null);
            echo "</select></dd>
\t</dl>
\t</fieldset>

\t";
            // line 53
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["loops"] ?? null), "backend", [], "any", false, false, false, 53));
            foreach ($context['_seq'] as $context["_key"] => $context["backend"]) {
                // line 54
                echo "
\t\t<fieldset id=\"search_";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["backend"], "IDENTIFIER", [], "any", false, false, false, 55);
                echo "_settings\">
\t\t\t<legend>";
                // line 56
                echo twig_get_attribute($this->env, $this->source, $context["backend"], "NAME", [], "any", false, false, false, 56);
                echo "</legend>
\t\t";
                // line 57
                echo twig_get_attribute($this->env, $this->source, $context["backend"], "SETTINGS", [], "any", false, false, false, 57);
                echo "
\t\t</fieldset>

\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['backend'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "
\t<fieldset class=\"submit-buttons\">
\t\t<legend>";
            // line 63
            echo $this->extensions['phpbb\template\twig\extension']->lang("SUBMIT");
            echo "</legend>
\t\t<input class=\"button1\" type=\"submit\" id=\"submit\" name=\"submit\" value=\"";
            // line 64
            echo $this->extensions['phpbb\template\twig\extension']->lang("SUBMIT");
            echo "\" />&nbsp;
\t\t<input class=\"button2\" type=\"reset\" id=\"reset\" name=\"reset\" value=\"";
            // line 65
            echo $this->extensions['phpbb\template\twig\extension']->lang("RESET");
            echo "\" />
\t\t";
            // line 66
            echo ($context["S_FORM_TOKEN"] ?? null);
            echo "
\t</fieldset>
\t</form>

";
        } elseif (        // line 70
($context["S_INDEX"] ?? null)) {
            // line 71
            echo "
\t<script>
\t// <![CDATA[
\t\t/**
\t\t* Popup search progress bar
\t\t*/
\t\tfunction popup_progress_bar(progress_type)
\t\t{
\t\t\tclose_waitscreen = 0;
\t\t\t// no scrollbars
\t\t\tpopup('";
            // line 81
            echo ($context["UA_PROGRESS_BAR"] ?? null);
            echo "&amp;type=' + progress_type, 400, 240, '_index');
\t\t}
\t// ]]>
\t</script>

\t<h1>";
            // line 86
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SEARCH_INDEX");
            echo "</h1>

\t";
            // line 88
            if (($context["S_CONTINUE_INDEXING"] ?? null)) {
                // line 89
                echo "\t\t<p>";
                echo $this->extensions['phpbb\template\twig\extension']->lang("CONTINUE_EXPLAIN");
                echo "</p>

\t\t<form id=\"acp_search_continue\" method=\"post\" action=\"";
                // line 91
                echo ($context["U_CONTINUE_INDEXING"] ?? null);
                echo "\">
\t\t\t<fieldset class=\"submit-buttons\">
\t\t\t\t<legend>";
                // line 93
                echo $this->extensions['phpbb\template\twig\extension']->lang("SUBMIT");
                echo "</legend>
\t\t\t\t<input class=\"button1\" type=\"submit\" id=\"continue\" name=\"continue\" value=\"";
                // line 94
                echo $this->extensions['phpbb\template\twig\extension']->lang("CONTINUE");
                echo "\" onclick=\"popup_progress_bar('";
                echo ($context["S_CONTINUE_INDEXING"] ?? null);
                echo "');\" />&nbsp;
\t\t\t\t<input class=\"button2\" type=\"submit\" id=\"cancel\" name=\"cancel\" value=\"";
                // line 95
                echo $this->extensions['phpbb\template\twig\extension']->lang("CANCEL");
                echo "\" />
\t\t\t\t";
                // line 96
                echo ($context["S_FORM_TOKEN"] ?? null);
                echo "
\t\t\t</fieldset>
\t\t</form>
\t";
            } else {
                // line 100
                echo "
\t\t<p>";
                // line 101
                echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SEARCH_INDEX_EXPLAIN");
                echo "</p>

\t\t";
                // line 103
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["loops"] ?? null), "backend", [], "any", false, false, false, 103));
                foreach ($context['_seq'] as $context["_key"] => $context["backend"]) {
                    // line 104
                    echo "
\t\t\t";
                    // line 105
                    if (twig_get_attribute($this->env, $this->source, $context["backend"], "S_STATS", [], "any", false, false, false, 105)) {
                        // line 106
                        echo "
\t\t\t<form id=\"acp_search_index_";
                        // line 107
                        echo twig_get_attribute($this->env, $this->source, $context["backend"], "NAME", [], "any", false, false, false, 107);
                        echo "\" method=\"post\" action=\"";
                        echo ($context["U_ACTION"] ?? null);
                        echo "\">

\t\t\t\t<fieldset class=\"tabulated\">

\t\t\t\t";
                        // line 111
                        echo twig_get_attribute($this->env, $this->source, $context["backend"], "S_HIDDEN_FIELDS", [], "any", false, false, false, 111);
                        echo "

\t\t\t\t<legend>";
                        // line 113
                        echo $this->extensions['phpbb\template\twig\extension']->lang("INDEX_STATS");
                        echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["backend"], "L_NAME", [], "any", false, false, false, 113);
                        echo " ";
                        if (twig_get_attribute($this->env, $this->source, $context["backend"], "S_ACTIVE", [], "any", false, false, false, 113)) {
                            echo "(";
                            echo $this->extensions['phpbb\template\twig\extension']->lang("ACTIVE");
                            echo ") ";
                        }
                        echo "</legend>

\t\t\t\t<table class=\"table1\">
\t\t\t\t\t<caption>";
                        // line 116
                        echo twig_get_attribute($this->env, $this->source, $context["backend"], "L_NAME", [], "any", false, false, false, 116);
                        echo " ";
                        if (twig_get_attribute($this->env, $this->source, $context["backend"], "S_ACTIVE", [], "any", false, false, false, 116)) {
                            echo "(";
                            echo $this->extensions['phpbb\template\twig\extension']->lang("ACTIVE");
                            echo ") ";
                        }
                        echo "</caption>
\t\t\t\t\t<col class=\"col1\" /><col class=\"col2\" /><col class=\"col1\" /><col class=\"col2\" />
\t\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>";
                        // line 120
                        echo $this->extensions['phpbb\template\twig\extension']->lang("STATISTIC");
                        echo "</th>
\t\t\t\t\t<th>";
                        // line 121
                        echo $this->extensions['phpbb\template\twig\extension']->lang("VALUE");
                        echo "</th>
\t\t\t\t\t<th>";
                        // line 122
                        echo $this->extensions['phpbb\template\twig\extension']->lang("STATISTIC");
                        echo "</th>
\t\t\t\t\t<th>";
                        // line 123
                        echo $this->extensions['phpbb\template\twig\extension']->lang("VALUE");
                        echo "</th>
\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t";
                        // line 127
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["backend"], "data", [], "any", false, false, false, 127));
                        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                            // line 128
                            echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>";
                            // line 129
                            echo twig_get_attribute($this->env, $this->source, $context["data"], "STATISTIC_1", [], "any", false, false, false, 129);
                            echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
                            echo "</td>
\t\t\t\t\t\t<td>";
                            // line 130
                            echo twig_get_attribute($this->env, $this->source, $context["data"], "VALUE_1", [], "any", false, false, false, 130);
                            echo "</td>
\t\t\t\t\t\t<td>";
                            // line 131
                            echo twig_get_attribute($this->env, $this->source, $context["data"], "STATISTIC_2", [], "any", false, false, false, 131);
                            if (twig_get_attribute($this->env, $this->source, $context["data"], "STATISTIC_2", [], "any", false, false, false, 131)) {
                                echo $this->extensions['phpbb\template\twig\extension']->lang("COLON");
                            }
                            echo "</td>
\t\t\t\t\t\t<td>";
                            // line 132
                            echo twig_get_attribute($this->env, $this->source, $context["data"], "VALUE_2", [], "any", false, false, false, 132);
                            echo "</td>
\t\t\t\t\t</tr>
\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 135
                        echo "\t\t\t\t</tbody>
\t\t\t\t</table>

\t\t\t";
                    }
                    // line 139
                    echo "
\t\t\t<p class=\"quick\">
\t\t\t";
                    // line 141
                    if (twig_get_attribute($this->env, $this->source, $context["backend"], "S_INDEXED", [], "any", false, false, false, 141)) {
                        // line 142
                        echo "\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"delete\" />
\t\t\t\t<input class=\"button2\" type=\"submit\" value=\"";
                        // line 143
                        echo $this->extensions['phpbb\template\twig\extension']->lang("DELETE_INDEX");
                        echo "\" onclick=\"popup_progress_bar('delete');\" />
\t\t\t";
                    } else {
                        // line 145
                        echo "\t\t\t\t<input type=\"hidden\" name=\"action\" value=\"create\" />
\t\t\t\t<input class=\"button2\" type=\"submit\" value=\"";
                        // line 146
                        echo $this->extensions['phpbb\template\twig\extension']->lang("CREATE_INDEX");
                        echo "\" onclick=\"popup_progress_bar('create');\" />
\t\t\t";
                    }
                    // line 148
                    echo "\t\t\t</p>
\t\t\t";
                    // line 149
                    echo ($context["S_FORM_TOKEN"] ?? null);
                    echo "
\t\t\t</fieldset>

\t\t\t</form>
\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['backend'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 154
                echo "
\t";
            }
            // line 156
            echo "
";
        }
        // line 158
        echo "
";
        // line 159
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "acp_search.html", 159)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "acp_search.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  483 => 159,  480 => 158,  476 => 156,  472 => 154,  461 => 149,  458 => 148,  453 => 146,  450 => 145,  445 => 143,  442 => 142,  440 => 141,  436 => 139,  430 => 135,  421 => 132,  414 => 131,  410 => 130,  405 => 129,  402 => 128,  398 => 127,  391 => 123,  387 => 122,  383 => 121,  379 => 120,  366 => 116,  351 => 113,  346 => 111,  337 => 107,  334 => 106,  332 => 105,  329 => 104,  325 => 103,  320 => 101,  317 => 100,  310 => 96,  306 => 95,  300 => 94,  296 => 93,  291 => 91,  285 => 89,  283 => 88,  278 => 86,  270 => 81,  258 => 71,  256 => 70,  249 => 66,  245 => 65,  241 => 64,  237 => 63,  233 => 61,  223 => 57,  219 => 56,  215 => 55,  212 => 54,  208 => 53,  201 => 49,  194 => 48,  189 => 46,  179 => 41,  172 => 40,  166 => 37,  159 => 36,  153 => 33,  146 => 32,  140 => 29,  133 => 28,  125 => 25,  118 => 24,  110 => 21,  103 => 20,  93 => 17,  85 => 16,  78 => 15,  73 => 13,  67 => 10,  62 => 8,  56 => 6,  54 => 5,  49 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "acp_search.html", "");
    }
}
