<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* viewtopic_topic_tools.html */
class __TwigTemplate_b83314593057d75f2a547ec079fca9bc4b9074b49d2652da12c5a22549ced198 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ( !($context["S_IS_BOT"] ?? null)) {
            // line 2
            echo "\t";
            // line 3
            echo "\t";
            if (($context["U_WATCH_TOPIC"] ?? null)) {
                // line 4
                echo "\t\t<a href=\"";
                echo ($context["U_WATCH_TOPIC"] ?? null);
                echo "\" class=\"button\" title=\"";
                echo ($context["S_WATCH_TOPIC_TITLE"] ?? null);
                echo "\" id=\"topic_watch_button\">
\t\t\t<span>";
                // line 5
                echo ($context["S_WATCH_TOPIC_TITLE"] ?? null);
                echo "</span> <i class=\"icon fa-";
                if (($context["S_WATCHING_TOPIC"] ?? null)) {
                    echo "check-";
                }
                echo "square-o fa-fw\" aria-hidden=\"true\"></i>
\t\t</a>
\t";
            }
            // line 8
            echo "\t<span class=\"responsive-hide\">
\t\t";
            // line 9
            if (($context["U_BOOKMARK_TOPIC"] ?? null)) {
                // line 10
                echo "\t\t\t<a href=\"";
                echo ($context["U_BOOKMARK_TOPIC"] ?? null);
                echo "\" class=\"button\" title=\"";
                echo $this->extensions['phpbb\template\twig\extension']->lang("BOOKMARK_TOPIC");
                echo "\" id=\"topic_bookmark_button\">
\t\t\t\t\t<span>";
                // line 11
                echo ($context["S_BOOKMARK_TOPIC"] ?? null);
                echo "</span> <i class=\"icon fa-bookmark-o fa-fw\" aria-hidden=\"true\"></i>
\t\t\t</a>
\t\t";
            }
            // line 14
            echo "\t\t";
            if (($context["U_BUMP_TOPIC"] ?? null)) {
                // line 15
                echo "\t\t\t<a href=\"";
                echo ($context["U_BUMP_TOPIC"] ?? null);
                echo "\" class=\"button\" title=\"";
                echo $this->extensions['phpbb\template\twig\extension']->lang("BUMP_TOPIC");
                echo "\" id=\"topic_bump_button\">
\t\t\t\t\t<span>";
                // line 16
                echo $this->extensions['phpbb\template\twig\extension']->lang("BUMP_TOPIC");
                echo "</span> <i class=\"icon fa-level-up fa-fw\" aria-hidden=\"true\"></i>
\t\t\t</a>
\t\t";
            }
            // line 19
            echo "\t</span>
\t";
            // line 20
            if ((((((($context["U_WATCH_TOPIC"] ?? null) || ($context["U_BOOKMARK_TOPIC"] ?? null)) || ($context["U_BUMP_TOPIC"] ?? null)) || ($context["U_EMAIL_TOPIC"] ?? null)) || ($context["U_PRINT_TOPIC"] ?? null)) || ($context["S_DISPLAY_TOPIC_TOOLS"] ?? null))) {
                // line 21
                echo "\t\t<div class=\"dropdown-container dropdown-button-control topic-tools responsive-show\"";
                if ( !(((($context["U_BUMP_TOPIC"] ?? null) || ($context["U_EMAIL_TOPIC"] ?? null)) || ($context["U_PRINT_TOPIC"] ?? null)) || ($context["S_DISPLAY_TOPIC_TOOLS"] ?? null))) {
                    echo " style=\"display: none;\"";
                }
                echo ">
\t\t\t<span title=\"";
                // line 22
                echo $this->extensions['phpbb\template\twig\extension']->lang("TOPIC_TOOLS");
                echo "\" class=\"button button-secondary dropdown-trigger dropdown-select\">
\t\t\t\t<i class=\"icon fa-wrench fa-fw\" aria-hidden=\"true\"></i>
\t\t\t\t<span class=\"caret\"><i class=\"icon fa-sort-down fa-fw\" aria-hidden=\"true\"></i></span>
\t\t\t</span>
\t\t\t<div class=\"dropdown\">
\t\t\t\t<div class=\"pointer\">
\t\t\t\t\t<div class=\"pointer-inner\"></div>
\t\t\t\t</div>
\t\t\t\t<ul class=\"dropdown-contents\">
\t\t\t\t\t";
                // line 31
                // line 32
                echo "\t\t\t\t\t";
                if (($context["U_WATCH_TOPIC"] ?? null)) {
                    // line 33
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
                    // line 34
                    echo ($context["U_WATCH_TOPIC"] ?? null);
                    echo "\" class=\"watch-topic-link\" title=\"";
                    echo ($context["S_WATCH_TOPIC_TITLE"] ?? null);
                    echo "\" data-ajax=\"toggle_link\" data-toggle-class=\"icon ";
                    if (($context["S_WATCHING_TOPIC"] ?? null)) {
                        echo "fa-check-square-o";
                    } else {
                        echo "fa-square-o";
                    }
                    echo " fa-fw\" data-toggle-text=\"";
                    echo ($context["S_WATCH_TOPIC_TOGGLE"] ?? null);
                    echo "\" data-toggle-url=\"";
                    echo ($context["U_WATCH_TOPIC_TOGGLE"] ?? null);
                    echo "\" data-update-all=\".watch-topic-link\">
\t\t\t\t\t\t\t<i class=\"icon ";
                    // line 35
                    if (($context["S_WATCHING_TOPIC"] ?? null)) {
                        echo "fa-check-square-o";
                    } else {
                        echo "fa-square-o";
                    }
                    echo " fa-fw\" aria-hidden=\"true\"></i><span>";
                    echo ($context["S_WATCH_TOPIC_TITLE"] ?? null);
                    echo "</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 39
                echo "\t\t\t\t\t";
                if (($context["U_BOOKMARK_TOPIC"] ?? null)) {
                    // line 40
                    echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
                    // line 41
                    echo ($context["U_BOOKMARK_TOPIC"] ?? null);
                    echo "\" class=\"bookmark-link\" title=\"";
                    echo $this->extensions['phpbb\template\twig\extension']->lang("BOOKMARK_TOPIC");
                    echo "\" data-ajax=\"alt_text\" data-alt-text=\"";
                    echo ($context["S_BOOKMARK_TOGGLE"] ?? null);
                    echo "\" data-update-all=\".bookmark-link\">
\t\t\t\t\t\t\t\t<i class=\"icon fa-bookmark-o fa-fw\" aria-hidden=\"true\"></i><span>";
                    // line 42
                    echo ($context["S_BOOKMARK_TOPIC"] ?? null);
                    echo "</span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t\t";
                if (($context["U_BUMP_TOPIC"] ?? null)) {
                    // line 47
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
                    // line 48
                    echo ($context["U_BUMP_TOPIC"] ?? null);
                    echo "\" title=\"";
                    echo $this->extensions['phpbb\template\twig\extension']->lang("BUMP_TOPIC");
                    echo "\" data-ajax=\"true\">
\t\t\t\t\t\t\t<i class=\"icon fa-level-up fa-fw\" aria-hidden=\"true\"></i><span>";
                    // line 49
                    echo $this->extensions['phpbb\template\twig\extension']->lang("BUMP_TOPIC");
                    echo "</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 53
                echo "\t\t\t\t\t";
                if (($context["U_EMAIL_TOPIC"] ?? null)) {
                    // line 54
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
                    // line 55
                    echo ($context["U_EMAIL_TOPIC"] ?? null);
                    echo "\" title=\"";
                    echo $this->extensions['phpbb\template\twig\extension']->lang("EMAIL_TOPIC");
                    echo "\">
\t\t\t\t\t\t\t<i class=\"icon fa-envelope-o fa-fw\" aria-hidden=\"true\"></i><span>";
                    // line 56
                    echo $this->extensions['phpbb\template\twig\extension']->lang("EMAIL_TOPIC");
                    echo "</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 60
                echo "\t\t\t\t\t";
                if (($context["U_PRINT_TOPIC"] ?? null)) {
                    // line 61
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
                    // line 62
                    echo ($context["U_PRINT_TOPIC"] ?? null);
                    echo "\" title=\"";
                    echo $this->extensions['phpbb\template\twig\extension']->lang("PRINT_TOPIC");
                    echo "\" accesskey=\"p\">
\t\t\t\t\t\t\t<i class=\"icon fa-print fa-fw\" aria-hidden=\"true\"></i><span>";
                    // line 63
                    echo $this->extensions['phpbb\template\twig\extension']->lang("PRINT_TOPIC");
                    echo "</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 67
                echo "\t\t\t\t\t";
                // line 68
                echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "viewtopic_topic_tools.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 68,  233 => 67,  226 => 63,  220 => 62,  217 => 61,  214 => 60,  207 => 56,  201 => 55,  198 => 54,  195 => 53,  188 => 49,  182 => 48,  179 => 47,  176 => 46,  169 => 42,  161 => 41,  158 => 40,  155 => 39,  142 => 35,  126 => 34,  123 => 33,  120 => 32,  119 => 31,  107 => 22,  100 => 21,  98 => 20,  95 => 19,  89 => 16,  82 => 15,  79 => 14,  73 => 11,  66 => 10,  64 => 9,  61 => 8,  51 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "viewtopic_topic_tools.html", "");
    }
}
